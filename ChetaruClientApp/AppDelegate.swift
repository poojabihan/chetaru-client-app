//
//  AppDelegate.swift
//  ChetaruClientApp
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
let kConstantObj = kConstant()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 255.0/255.0, green: 15.0/255.0, blue: 44.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]

        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -80.0), for: .default)
        
        let defaults = UserDefaults.standard
        if AuthModel.sharedInstance.strToken.isEmpty {
            if defaults.value(forKey: "token") != nil {
                
                //direct login
                setLoginData()
                
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let rootController = storyboard.instantiateViewController(withIdentifier: "InvoiceListViewControllerNav")
//                    self.window = UIWindow(frame: UIScreen.main.bounds)
//                    self.window?.rootViewController = rootController
//                    self.window?.makeKeyAndVisible()
                
                let mainVcIntial = kConstantObj.SetIntialMainViewController("InvoiceListViewController")
                self.window?.rootViewController = mainVcIntial

                
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    //MARK: - Custom Methods
    func setLoginData() {
        
        let defaults = UserDefaults.standard
        let auth = AuthModel.sharedInstance
        
        auth.strID = defaults.value(forKey: "id") as! String
        auth.strName = defaults.value(forKey: "name") as! String
        auth.strEmail = defaults.value(forKey: "email") as! String
        auth.strMobile = defaults.value(forKey: "mobile") as! String
        auth.strAddress = defaults.value(forKey: "address") as! String
        auth.strToken = defaults.value(forKey: "token") as! String
        auth.strImage = defaults.value(forKey: "image") as! String
        
    }
}

