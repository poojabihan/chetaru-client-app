//
//  OrganisationListViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 01/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class OrganisationListViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblOrg: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnMenu: UIButton!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrOrg = [OrganisationModel]()
    var arrFilteredOrg = [OrganisationModel]()
    var addInvoiceModel = AddInvoiceModel()
    var objAddInvoice = AddInvoiceViewController()
    var type = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.showsScopeBar = true
        searchBar.delegate = self

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        callWebServiceForOrgList()
        searchBar.text = ""
        if type == "" {
            btnAdd.isHidden = false
            btnMenu.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        }
        else {
            btnAdd.isHidden = true
            btnMenu.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action Methods
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        if type == "" {
            sideMenuVC.toggleMenu()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilteredOrg = searchText.isEmpty ? arrOrg : arrOrg.filter { (item: OrganisationModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblOrg.reloadData()
    }
    
    //MARK: - WebService Methods
    func callWebServiceForOrgList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.orgList, param: nil, withHeader: false ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    ParserController.parseOrganisationList(response: response!, completionHandler: { (arr) in
                        self.arrOrg = arr
                        self.arrFilteredOrg = arr
                        self.tblOrg.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilteredOrg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrganisationTableViewCell") as! OrganisationTableViewCell
        cell.selectionStyle = .none
        cell.lblName.text = arrFilteredOrg[indexPath.row].strName
        cell.lblEmail.text = arrFilteredOrg[indexPath.row].strEmail
        cell.lblWebsite.text = arrFilteredOrg[indexPath.row].strWebsite
        cell.lblPhone.text = arrFilteredOrg[indexPath.row].strPhone
        cell.lblAddress.text = arrFilteredOrg[indexPath.row].strAddress
        
//        let url = URL(string: arrFilteredOrg[indexPath.row].strImage)
//        cell.imgOrg.kf.setImage(with: url, placeholder: UIImage(named : #imageLiteral(resourceName: "placeholder")), progressBlock:nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if type == kOrgToBill {
            
            addInvoiceModel.strBillToAddress = arrFilteredOrg[indexPath.row].strAddress
            addInvoiceModel.strBillToCIN = arrFilteredOrg[indexPath.row].strCIN
            addInvoiceModel.strBillToPAN = arrFilteredOrg[indexPath.row].strPAN
            addInvoiceModel.strBillToGSTIN = arrFilteredOrg[indexPath.row].strGSTIN
            
            self.navigationController?.popViewController(animated: true)
        }
        else if type == kOrgAddInvoiceSA {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootController = storyboard.instantiateViewController(withIdentifier: "AddInvoiceViewController") as? AddInvoiceViewController
            let model = AddInvoiceModel()
            model.strOrgAddress = arrFilteredOrg[indexPath.row].strAddress
            model.strOrgCIN = arrFilteredOrg[indexPath.row].strCIN
            model.strOrgPAN = arrFilteredOrg[indexPath.row].strPAN
            model.strOrgGSTIN = arrFilteredOrg[indexPath.row].strGSTIN
            model.strOrgCurrency = arrFilteredOrg[indexPath.row].strCurrencySymbol

            rootController?.addInvoiceModel = model
            
            self.navigationController?.pushViewController(rootController!, animated: true)
        }
        else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
            
            rootController?.orgModel = arrFilteredOrg[indexPath.row]
            
            self.navigationController?.pushViewController(rootController!, animated: true)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
