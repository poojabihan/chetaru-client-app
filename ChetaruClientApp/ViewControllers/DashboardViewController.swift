//
//  DashboardViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 05/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var imgOrg: UIImageView!

    //MARK: - Variables
    var orgModel = OrganisationModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblOrgName.text = orgModel.strName
//        let url = URL(string: orgModel.strImage)
//        imgOrg.kf.setImage(with: url, placeholder: UIImage(named : #imageLiteral(resourceName: "placeholder")), progressBlock:nil, completionHandler: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action Methods
    @IBAction func btnInvoiveAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "InvoiceListViewController") as? InvoiceListViewController
        
        let filterModel = FilterModel()
        filterModel.arrOrganisations.append(orgModel)
        rootController?.filterModel = filterModel
        rootController?.type = kInvoiceWithoutFiter

        self.navigationController?.pushViewController(rootController!, animated: true)
    }
    
    @IBAction func btnFeedbackAction(_ sender: UIButton) {
    }
    
    @IBAction func btnReportsAction(_ sender: UIButton) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
