//
//  AddItemViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 01/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class AddItemViewController: UIViewController, UITextViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var txtItemDesc: UITextView!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtRate: UITextField!
    @IBOutlet weak var txtIGST: UITextField!
    @IBOutlet weak var tapView: UIView!

    //MARK: - Variables
    var objAddInvoice = AddInvoiceViewController()
    let panel = JKNotificationPanel()
    var addInvoiceModel = AddInvoiceModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tapView.addGestureRecognizer(tap)
        
        txtItemDesc.text = "Item Description:"
        txtItemDesc.textColor = UIColor.lightGray
        txtItemDesc.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Item Description:"
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- Custom Methods
    @objc func viewTapped() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK:- Action Methods
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        if txtItemDesc.text.isEmpty {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter item description.")
        }
        else if (txtQuantity.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter quantity.")
        }
        else if (txtRate.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter rate.")
        }
        else if (txtIGST.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter IGST.")
        }
        else {
            let model = ItemModel()
            model.strItemDesc = txtItemDesc.text
            model.strQuantity = txtQuantity.text!
            model.strRate = txtRate.text!
            model.strGST = txtIGST.text!
            
            let qty = Double(txtQuantity.text!)
            let rate = Double(txtRate.text!)
            let igst = Double(txtIGST.text!)

            let amt = qty! * rate!
            let gst = (amt * igst!) / 100
            let finaAmt = amt + gst
            
            model.strAmount = String(finaAmt)
            
            addInvoiceModel.arrItems.append(model)
            
            var subTotal = 0.0
            var gsto = 0.0

            for value in addInvoiceModel.arrItems {
                
                subTotal = subTotal + Double(value.strAmount)!
                gsto = gsto + Double(value.strGST)!
            }
            
            addInvoiceModel.strSubTotal = String(subTotal)
            addInvoiceModel.strGSTO = String(gsto)
            
            let finalGst = (subTotal * gsto) / 100
            let finaTotal = subTotal + finalGst
            
            addInvoiceModel.strTotal = String(finaTotal)
            addInvoiceModel.strBalanceDue = String(finaTotal)

        }
        
        objAddInvoice.refreshScreen()
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
