//
//  AddInvoiceViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class AddInvoiceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    //MARK: - IBOutlets
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tblAddInvoice: UITableView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var strInvoiceNumber = ""
    var addInvoiceModel = AddInvoiceModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tblAddInvoice.tableFooterView = footerView
        callWebServiceForInvoiceNumber()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblAddInvoice.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnSaveAction() {
        
        if addInvoiceModel.strDueDate == "" {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Due Date.")
        }
        else if addInvoiceModel.strTerms == "" {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Terms.")
        }
        else if addInvoiceModel.strBillToAddress == "" {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select Bill To Organisation.")
        }
        else if addInvoiceModel.arrItems.count == 0 {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please add atleast one item.")
        }
        else {
            
        }
        
    }

    
    //MARK: - Custom Methods
    func refreshScreen() {
        tblAddInvoice.reloadData()
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return addInvoiceModel.arrItems.count
        default:
            if addInvoiceModel.arrItems.count > 0 {
                return 2
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceHeaderTableViewCell") as! InvoiceHeaderTableViewCell
                cell.selectionStyle = .none
                cell.lblInvoiceID.text = addInvoiceModel.strInvoiceNumber
                cell.lblInvoiceDate.text = Date().toString(dateFormat: "dd, MMM yyyy")
                cell.addInvoiceModel = addInvoiceModel

                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiveOrganisationTableViewCell") as! InvoiveOrganisationTableViewCell
                cell.lblAddress.text = addInvoiceModel.strOrgAddress
                cell.lblCIN.text = addInvoiceModel.strOrgCIN
                cell.lblPAN.text = addInvoiceModel.strOrgPAN
                cell.lblGSTIN.text = addInvoiceModel.strOrgGSTIN

                cell.selectionStyle = .none

                return cell
            case 2:
                
                if addInvoiceModel.strBillToAddress == "" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellOrgBillTo")
                    cell?.selectionStyle = .none

                    return cell!
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiveOrganisationTableViewCellBillTo") as! InvoiveOrganisationTableViewCell
                    cell.selectionStyle = .none
                    cell.lblAddress.text = addInvoiceModel.strBillToAddress
                    cell.lblCIN.text = addInvoiceModel.strBillToCIN
                    cell.lblPAN.text = addInvoiceModel.strBillToPAN
                    cell.lblGSTIN.text = addInvoiceModel.strBillToGSTIN
                    
                    return cell
                }
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                cell?.selectionStyle = .none

                return cell!
            }
            
        }
            
        else  if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceItemsTableViewCell") as! InvoiceItemsTableViewCell
            cell.selectionStyle = .none
            cell.lblDesc.text = addInvoiceModel.arrItems[indexPath.row].strItemDesc
            cell.lblQuantity.text = addInvoiceModel.arrItems[indexPath.row].strQuantity
            cell.lblRate.text = addInvoiceModel.arrItems[indexPath.row].strRate
            cell.lblGST.text = addInvoiceModel.arrItems[indexPath.row].strGST + "%"
            cell.lblAmount.text = addInvoiceModel.strOrgCurrency + " " + addInvoiceModel.arrItems[indexPath.row].strAmount

            return cell
        }
            
        else {
            
            if addInvoiceModel.arrItems.count > 0 {
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTotalTableViewCell") as! InvoiceTotalTableViewCell
                    cell.selectionStyle = .none
                    cell.lblSubTotal.text = addInvoiceModel.strOrgCurrency + " " + addInvoiceModel.strSubTotal
                    cell.lblGSTO.text = addInvoiceModel.strGSTO + "%"
                    cell.lblTotal.text = addInvoiceModel.strOrgCurrency + " " + addInvoiceModel.strTotal
                    cell.lblBalanceDue.text = addInvoiceModel.strOrgCurrency + " " + addInvoiceModel.strBalanceDue
                    
                    return cell
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceNoteTableViewCell") as! InvoiceNoteTableViewCell
                    cell.selectionStyle = .none
                    cell.addInvoiceModel = addInvoiceModel
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                    cell?.selectionStyle = .none
                    
                    return cell!
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceNoteTableViewCell") as! InvoiceNoteTableViewCell
                cell.selectionStyle = .none
                
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = UIView()
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "CellItemListing")
            headerView.addSubview(headerCell!)
            return headerView
        }
        else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50.0
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                return 280
            case 1:
                return UITableViewAutomaticDimension
            case 2:
                if addInvoiceModel.strBillToAddress == "" {
                    return 50
                }
                else {
                    return UITableViewAutomaticDimension
                }
            default:
                return UITableViewAutomaticDimension
            }
        }
        else if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        }
        else if indexPath.section == 2 {
            
            if addInvoiceModel.arrItems.count > 0 {
                if indexPath.row == 0 {
                    return 140
                }
                return 120
            }
            else {
                return 120
            }
            
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForInvoiceNumber() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.invoiceNumber, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.addInvoiceModel.strInvoiceNumber = response!["data"]["invoice_id"].stringValue
                    self.tblAddInvoice.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "toAddItem" {
            let vc = segue.destination as! AddItemViewController
            vc.objAddInvoice = self
            vc.addInvoiceModel = addInvoiceModel
        }
        else if segue.identifier == "toOrgBillTo" {
            let vc = segue.destination as! OrganisationListViewController
            vc.objAddInvoice = self
            vc.addInvoiceModel = addInvoiceModel
            vc.type = kOrgToBill
        }
     }
    
    
}
