//
//  InvoiceListViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 26/06/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class InvoiceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblInvoiceList: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnAdd: UIButton!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrInvoices = [InvoiceModel]()
    var arrFilterdInvoices = [InvoiceModel]()
    var filterModel = FilterModel()
    var type = ""

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.showsScopeBar = true
        searchBar.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if type == kInvoiceWithoutFiter {
            btnFilter.isHidden = true
            btnAdd.isHidden = true

            btnMenu.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
        else {
            filterModel = AuthModel.sharedInstance.objFilterModel
        }
        
//        // remove left buttons (in case you added some)
//        self.navigationItem.leftBarButtonItems = []
//        // hide the default back buttons
//        self.navigationItem.hidesBackButton = true
        tblInvoiceList.tableFooterView = UIView()
        searchBar.text = ""
        
//        searchBar.tintColor = navigationController?.navigationBar.tintColor
//        searchBar.backgroundColor = navigationController?.navigationBar.tintColor
//        searchBar.barTintColor = UIColor(red: 255.0/255.0, green: 15.0/255.0, blue: 44.0/255.0, alpha: 1.0)

        self.navigationController?.setNavigationBarHidden(false, animated: false)

        callWebServiceForInvoiceList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilterdInvoices = searchText.isEmpty ? arrInvoices : arrInvoices.filter { (item: InvoiceModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strInvoiceId.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblInvoiceList.reloadData()
    }
    
    //MARK:- Action Methods
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        if type == kInvoiceWithoutFiter {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            sideMenuVC.toggleMenu()
        }
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "FilterViewController")
        self.navigationController?.pushViewController(rootController, animated: true)
    }

    @IBAction func btnAddInvoiceAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "OrganisationListViewController") as! OrganisationListViewController
        rootController.type = kOrgAddInvoiceSA
        self.navigationController?.pushViewController(rootController, animated: true)
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInvoices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTableViewCell") as! InvoiceTableViewCell
        
        cell.lblID.text = "ID: " + arrInvoices[indexPath.row].strInvoiceId
        cell.lblOrgName.text = "Organisation: " + arrInvoices[indexPath.row].strName
        cell.lblDueDate.text = arrInvoices[indexPath.row].strDueDate
        cell.lblStatus.text = arrInvoices[indexPath.row].strStatus
        
        cell.selectionStyle = .none
        
        if arrInvoices[indexPath.row].strStatus == "Complete" {
            cell.lblStatusColor.backgroundColor = UIColor(red: 0.0/255.0, green: 165.0/255.0, blue: 234.0/255.0, alpha: 1.0)
        }
        else {
            cell.lblStatusColor.backgroundColor = UIColor(red: 218.0/255.0, green: 57.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    //MARK: - WebService Methods
    func callWebServiceForInvoiceList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var arrOrg = [String]()
        for value in filterModel.arrOrganisations {
            arrOrg.append(value.strId)
        }
        
        let param = ["status":filterModel.strStatus,
                     "orgId":arrOrg] as [String : Any]
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.invoiceList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ParserController.parseInvoiceList(response: response!, completionHandler: { (arr) in
                        self.arrInvoices = arr
                        self.arrFilterdInvoices = arr
                        self.tblInvoiceList.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
