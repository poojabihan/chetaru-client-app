//
//  FilterViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 30/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var tblSubCategory: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchView: UIView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrCategory = [String]()
    var arrStatus = [String]()
    var arrOrg = [OrganisationModel]()
    var arrFilteredOrg = [OrganisationModel]()
    var selectedCategory = 0
    var selectedStatus = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        arrCategory = ["Organisations", "Status"]
        arrStatus = ["Complete", "Pending"]

        searchBar.showsScopeBar = true
        searchBar.delegate = self
        tblCategory.tableFooterView = UIView()
        tblSubCategory.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AuthModel.sharedInstance.objFilterModel.strStatus != "" {
            selectedStatus = arrStatus.index(of: AuthModel.sharedInstance.objFilterModel.strStatus)!
        }
        
        callWebServiceForOrgList()
        searchBar.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action Methods
    @IBAction func btnClearAllAction(_ sender: UIButton) {
        let model = FilterModel()
        model.strStatus = ""
        model.arrOrganisations.removeAll()
        
        let auth = AuthModel.sharedInstance
        auth.objFilterModel = model
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyAction(_ sender: UIButton) {
        
        let model = FilterModel()
        model.strStatus = selectedStatus != -1 ? arrStatus[selectedStatus] : ""
        for value in arrOrg {
            if value.isSelected {
                model.arrOrganisations.append(value)
            }
        }
        
        let auth = AuthModel.sharedInstance
        auth.objFilterModel = model
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilteredOrg = searchText.isEmpty ? arrOrg : arrOrg.filter { (item: OrganisationModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblSubCategory.reloadData()
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCategory {
            return arrCategory.count
        }
        else {
            if selectedCategory == 0 {
                return arrFilteredOrg.count
            }
            else {
                return arrStatus.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCategory {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            cell?.selectionStyle = .none

            cell?.textLabel?.text = arrCategory[indexPath.row]
            
            if indexPath.row == selectedCategory {
                cell?.backgroundColor = UIColor.white
            }
            else {
                cell?.backgroundColor = UIColor.clear
            }
            
            return cell!
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as! FilterTableViewCell
            cell.selectionStyle = .none

            if selectedCategory == 0 {
                
                cell.lblTitle.text = arrFilteredOrg[indexPath.row].strName

                if arrFilteredOrg[indexPath.row].isSelected {
                    cell.imgSelected.image = #imageLiteral(resourceName: "selected")
                }
                else {
                    cell.imgSelected.image = #imageLiteral(resourceName: "notSelected")
                }
            }
            else {
                
                cell.lblTitle.text = arrStatus[indexPath.row]


                if selectedStatus == indexPath.row {
                    cell.imgSelected.image = #imageLiteral(resourceName: "selected")
                }
                else {
                    cell.imgSelected.image = #imageLiteral(resourceName: "notSelected")
                }
            }
            
            return cell

        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblCategory {
            
            selectedCategory = indexPath.row
            
            if indexPath.row == 0 {
                tblSubCategory.tableHeaderView = searchView
            }
            else {
                tblSubCategory.tableHeaderView = nil
            }
        }
        else {
            if selectedCategory == 0 {
                arrFilteredOrg[indexPath.row].isSelected = !arrFilteredOrg[indexPath.row].isSelected
            }
            else {
                selectedStatus = indexPath.row
            }
        }
        
        tblCategory.reloadData()
        tblSubCategory.reloadData()

    }
    
    //MARK: - WebService Methods
    func callWebServiceForOrgList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.orgList, param: nil, withHeader: false ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    ParserController.parseOrganisationList(response: response!, completionHandler: { (arr) in
                        self.arrOrg = arr
                        self.arrFilteredOrg = arr
                        self.tblSubCategory.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
