//
//  SignUpViewController.swift
//  ChetaruClientApp
//
//  Created by Apple on 25/06/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet var imgProfile: UIImageView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var selectedCountryID = ""
    var imagePicker = UIImagePickerController()
    var arrCompany = [OrganisationModel]()
    let CompanyPicker = UIPickerView()
    var selectedCompanyID = ""
    var arrCountry = [CountryModel]()
    let countryPicker = UIPickerView()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        CompanyPicker.delegate = self
        txtCompany.inputView = CompanyPicker
        countryPicker.delegate = self
        txtCountry.inputView = countryPicker

        callWebServiceForOrgList()
        callWebServiceForCountryList()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    /*Action Sheet Options Function for Uploading File*/
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }

    // convert images into base64 and keep them into string
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgProfile.image!, 1.0)!
        if let imageData = imgProfile.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnProfilePicAction(sender: Any) {
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { _ in
            // self.imageView.image = UIImage(named: "user_default")
        }))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUpAction() {
        if (txtFname.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter first name.")
        }
        else if (txtLname.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter last name.")
        }
        else if (txtCompany.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter company name.")
        }
        else if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if !(txtEmail.text?.isValidEmail())! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
        else if (txtPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter password.")
        }
        else if (txtConfirmPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter confirm password.")
        }
        else if txtPassword.text != txtConfirmPassword.text {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password and Confirm password did not match.")
        }
        else if (txtPhone.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter phone number.")
        }
        else if (txtAddress.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter address.")
        }
        else if (txtCountry.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter country.")
        }
        else {
            callWebServiceForSignUp()
        }
    }
    
    // MARK:- UIPickerView Delegation
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == CompanyPicker {
            return arrCompany.count
        }
        return arrCountry.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == CompanyPicker {
            return arrCompany[row].strName
        }
        return arrCountry[row].strName

    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == CompanyPicker {
            txtCompany.text = arrCompany[row].strName
            selectedCompanyID = arrCompany[row].strId
        }
        txtCountry.text = arrCountry[row].strName
        selectedCountryID = arrCountry[row].strId

    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imgProfile.image = image
            self.dismiss(animated: false, completion: nil)
        }
            
        else{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }

    
    //MARK: - WebService Methods
    func callWebServiceForSignUp() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let name = txtFname.text! + txtLname.text!
        
        let param = ["name": name,
                     "organisation_id": selectedCompanyID,
                     "email": txtEmail.text ?? "",
                     "mobile": txtPhone.text ?? "",
                     "password": txtPassword.text ?? "",
                     "address": txtAddress.text ?? "",
                     "country": selectedCountryID,
                     "image": convertImageToBase64(image: imgProfile.image!)]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.register, param: param, withHeader: false ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForOrgList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.orgList, param: nil, withHeader: false ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    ParserController.parseOrganisationList(response: response!, completionHandler: { (arr) in
                        self.arrCompany = arr
                        self.CompanyPicker.reloadAllComponents()
                    })
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForCountryList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.countryList, param: nil, withHeader: false ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    ParserController.parseCountryList(response: response!, completionHandler: { (arr) in
                        self.arrCountry = arr
                        self.countryPicker.reloadAllComponents()
                    })
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
