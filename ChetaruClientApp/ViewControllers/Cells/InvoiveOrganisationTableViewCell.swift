//
//  InvoiveOrganisationTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiveOrganisationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCIN: UITextField!
    @IBOutlet weak var txtPAN: UITextField!
    @IBOutlet weak var txtGSTIN: UITextField!

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCIN: UILabel!
    @IBOutlet weak var lblPAN: UILabel!
    @IBOutlet weak var lblGSTIN: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
