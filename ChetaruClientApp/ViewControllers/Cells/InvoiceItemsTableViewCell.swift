//
//  InvoiceItemsTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceItemsTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblGST: UILabel!
    @IBOutlet weak var lblAmount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
