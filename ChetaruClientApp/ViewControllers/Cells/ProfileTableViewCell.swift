//
//  ProfileTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnImgView: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgCameraEdit: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
