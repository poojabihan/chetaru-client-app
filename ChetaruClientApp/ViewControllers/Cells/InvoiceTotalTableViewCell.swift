//
//  InvoiceTotalTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceTotalTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblGSTO: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblBalanceDue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
