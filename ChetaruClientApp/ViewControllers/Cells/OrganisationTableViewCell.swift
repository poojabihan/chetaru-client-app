//
//  OrganisationTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 01/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class OrganisationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgOrg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
