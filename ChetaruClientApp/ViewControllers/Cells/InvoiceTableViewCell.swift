//
//  InvoiceTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 29/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusColor: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
