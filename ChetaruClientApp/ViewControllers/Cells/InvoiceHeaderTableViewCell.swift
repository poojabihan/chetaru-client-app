//
//  InvoiceHeaderTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceHeaderTableViewCell: UITableViewCell, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var lblInvoiceID: UILabel!
    @IBOutlet weak var lblInvoiceDate: UILabel!
    @IBOutlet weak var txtDueDate: UITextField!
    @IBOutlet weak var txtTerms: UITextField!

    //MARK: - Variables
    var addInvoiceModel = AddInvoiceModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtDueDate.delegate = self
        txtTerms.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtDueDate {
            addInvoiceModel.strDueDate = txtDueDate.text!
        }
        else {
            addInvoiceModel.strTerms = txtTerms.text!
        }
    }
}
