//
//  InvoiceNoteTableViewCell.swift
//  ChetaruClientApp
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceNoteTableViewCell: UITableViewCell, UITextViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var txtNotes: UITextView!

    //MARK: - Variables
    var addInvoiceModel = AddInvoiceModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtNotes.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        addInvoiceModel.strNotes = textView.text
    }

}
