//
//  ParserController.swift
//  ChetaruClientApp
//
//  Created by Apple on 26/06/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class ParserController: NSObject {

   class func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.strID = json["data"]["id"].stringValue
        auth.strName = json["data"]["name"].stringValue
        auth.strEmail = json["data"]["email"].stringValue
        auth.strMobile = json["data"]["mobile"].stringValue
        auth.strAddress = json["data"]["address"].stringValue
        auth.strToken = json["data"]["token"].stringValue
        auth.strImage = json["data"]["image"].stringValue
        
        let defaults = UserDefaults.standard
        defaults.set(auth.strID, forKey: "id")
        defaults.set(auth.strName, forKey: "name")
        defaults.set(auth.strEmail, forKey: "email")
        defaults.set(auth.strMobile, forKey: "mobile")
        defaults.set(auth.strAddress, forKey: "address")
        defaults.set(auth.strToken, forKey: "token")
        defaults.set(auth.strImage, forKey: "image")
    }
    
    class func parseOrganisationList(response : JSON, completionHandler: @escaping ([OrganisationModel]) -> Void) {
        
        var arr = [OrganisationModel]()
        
        for json in response["data"].arrayValue {
            
            let model = OrganisationModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strAddress = json["address"].stringValue
            model.strEmail = json["email"].stringValue
            model.strWebsite = json["website"].stringValue
            model.strImage = json["image"].stringValue
            model.strCreatedAt = json["created_at"].stringValue
            model.strUpdatedAt = json["updated_at"].stringValue
            model.strPhone = json["phone"].stringValue
            model.strPAN = json["pan_number"].stringValue
            model.strCIN = json["cin_number"].stringValue
            model.strGSTIN = json["gstin_number"].stringValue
            model.strCurrencyCode = json["currency_code"].stringValue
            model.strCurrencyID = json["currency_id"].stringValue
            model.strCurrencySymbol = json["currency_symbol"].stringValue

            for value in AuthModel.sharedInstance.objFilterModel.arrOrganisations {
                if value.strId == json["id"].stringValue {
                    model.isSelected = true
                }
                else {
                    model.isSelected = false
                }
            }
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseCountryList(response : JSON, completionHandler: @escaping ([CountryModel]) -> Void) {
        
        var arr = [CountryModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CountryModel()
            
            model.strId = json["id"].stringValue
            model.strCode = json["country_code"].stringValue
            model.strName = json["country_name"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseInvoiceList(response : JSON, completionHandler: @escaping ([InvoiceModel]) -> Void) {
        
        var arr = [InvoiceModel]()
        
        for json in response["data"].arrayValue {
            
            let model = InvoiceModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strEmail = json["email"].stringValue
            model.strPhone = json["phone"].stringValue
            model.strAddress = json["address"].stringValue
            model.strImage = json["image"].stringValue
            model.strWebsite = json["website"].stringValue
            model.strInvoiceId = json["invoice_id"].stringValue
            model.strTerms = json["terms"].stringValue
            model.strOrganisationId = json["organisation_id"].stringValue
            model.strStatus = json["status"].stringValue
            model.strBillToOrganisationId = json["bill_to_organisation_id"].stringValue

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: json["due_date"].stringValue)
            dateFormatter.dateFormat = "dd, MMM yyyy"
            
            model.strDueDate = dateFormatter.string(from: date!)

            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseCurrencyList(response : JSON, completionHandler: @escaping ([CurrencyModel]) -> Void) {
        
        var arr = [CurrencyModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CurrencyModel()
            
            model.strID = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strCode = json["code"].stringValue
            model.strSymbol = json["symbol"].stringValue

            arr.append(model)
        }
        
        completionHandler(arr)
    }

}
