//
//  AuthModel.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class AuthModel {

    static let sharedInstance = AuthModel()

    var strID = ""
    var strName = ""
    var strEmail = ""
    var strMobile = ""
    var strAddress = ""
    var strToken = ""
    var strImage = ""
    var objFilterModel = FilterModel()

}
