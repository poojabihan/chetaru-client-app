//
//  AddInvoiceModel.swift
//  ChetaruClientApp
//
//  Created by Apple on 01/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AddInvoiceModel: NSObject {

    var strInvoiceNumber = ""
    var strInvoiceDate = ""
    var strDueDate = ""
    var strTerms = ""
    
    var strOrgAddress = ""
    var strOrgCIN = ""
    var strOrgPAN = ""
    var strOrgGSTIN = ""
    var strOrgCurrency = ""

    var strBillToAddress = ""
    var strBillToCIN = ""
    var strBillToPAN = ""
    var strBillToGSTIN = ""

    var arrItems = [ItemModel]()
    
    var strSubTotal = ""
    var strGSTO = ""
    var strTotal = ""
    var strBalanceDue = ""

    var strNotes = ""

}
