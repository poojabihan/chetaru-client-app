//
//  OrganisationModel.swift
//  ChetaruClientApp
//
//  Created by Apple on 26/06/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class OrganisationModel: NSObject {

    var strId = ""
    var strName = ""
    var strAddress = ""
    var strEmail = ""
    var strWebsite = ""
    var strImage = ""
    var strCreatedAt = ""
    var strUpdatedAt = ""
    var isSelected = false
    var strPhone = ""
    var strPAN = ""
    var strCIN = ""
    var strGSTIN = ""
    var strCurrencyCode = ""
    var strCurrencyID = ""
    var strCurrencySymbol = ""

}
