//
//  ItemModel.swift
//  ChetaruClientApp
//
//  Created by Apple on 01/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ItemModel: NSObject {

    var strItemDesc = ""
    var strQuantity = ""
    var strRate = ""
    var strGST = ""
    var strAmount = ""

}
