//
//  InvoiceModel.swift
//  ChetaruClientApp
//
//  Created by Apple on 29/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InvoiceModel: NSObject {

    var strId = ""
    var strName = ""
    var strEmail = ""
    var strPhone = ""
    var strAddress = ""
    var strImage = ""
    var strWebsite = ""
    var strInvoiceId = ""
    var strDueDate = ""
    var strTerms = ""
    var strOrganisationId = ""
    var strStatus = ""
    var strBillToOrganisationId = ""

}
