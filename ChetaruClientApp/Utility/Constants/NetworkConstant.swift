//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

let kBaseURL = "http://production.chetaru.co.uk/chetaru_client_app/api/"

let kOrgToBill = "ToBillMode"
let kOrgAddInvoiceSA = "SuperAdminForAddInvoice"
let kInvoiceWithFiter = "withFilter"
let kInvoiceWithoutFiter = "withoutFilter"

class NetworkConstant: NSObject {
    
    struct Auth {
        static let login = "login"
        static let forgotPassword = "forgotPassword"
        static let orgList = "organisations-list"
        static let countryList = "country-list"
        static let register = "register"
        static let invoiceList = "list-invoice"
        static let invoiceNumber = "getRandomInvoiceNumber"
        static let logout = "logout"
        static let addOrganisation = "add-organisation"
        static let listCurrency = "list-currency"

    }
}
