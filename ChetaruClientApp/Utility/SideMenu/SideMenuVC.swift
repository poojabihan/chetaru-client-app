
//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit
import JKNotificationPanel
//import NVActivityIndicatorView

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    let aData : NSArray = ["","Invoices","Organisations","Feedbacks","Reports","Logout"]
    let aImageView : NSArray = ["","Invoice","Organizations","Feedback","Reports","logout"]

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 255.0/255.0, green: 15.0/255.0, blue: 44.0/255.0, alpha: 1.0)

    }
    
    //MARK: - Custom Methods
    func clearUserDefaults() {
        
        let auth = AuthModel.sharedInstance
        auth.strID = ""
        auth.strName = ""
        auth.strEmail = ""
        auth.strMobile = ""
        auth.strAddress = ""
        auth.strToken = ""
        auth.strImage = ""
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "id")
        defaults.removeObject(forKey: "name")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "mobile")
        defaults.removeObject(forKey: "address")
        defaults.removeObject(forKey: "token")
        defaults.removeObject(forKey: "image")
        
    }
    
    //MARK: WebService Methods
    func callWebServiceLogout() {
        
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["":""]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: param, withHeader: true) { (response, errorMsg) in

//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {

                    self.clearUserDefaults()
                    let mainVcIntial = kConstantObj.SetIntialMainViewController("LoginViewController")
                    self.appDelegate.window?.rootViewController = mainVcIntial
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    //MARK: - UITableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let aCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            return aCell
        }
        else{
            let aCell = tableView.dequeueReusableCell(withIdentifier: "kCell", for: indexPath)
            let aLabel : UILabel = aCell.viewWithTag(10) as! UILabel
            aLabel.text = aData[indexPath.row] as? String
            
            let aImage : UIImageView = aCell.viewWithTag(20) as! UIImageView
            aImage.image = UIImage(named: aImageView[indexPath.row] as! String)

            return aCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.row {
        case 1:
            _ = kConstantObj.SetIntialMainViewController("InvoiceListViewController")
       case 2:
        _ = kConstantObj.SetIntialMainViewController("OrganisationListViewController") 
        case 3:
            print("Feedbacks")
//            _ = kConstantObj.SetIntialMainViewController("InvoiceListViewController")
        case 4:
            print("Reports")
//            _ = kConstantObj.SetIntialMainViewController("InvoiceListViewController")
        default:
            let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.callWebServiceLogout()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 185
        }
        else {
            return 60
        }
    }
}
